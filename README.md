# Reactivities Udemy Course repository

![Deploy status](https://github.com/trycatchlearn/Reactivities/actions/workflows/docker-push.yml/badge.svg)

This is the updated repository for the .Net 7.0, React 18 and React Router 6 version of the course refreshed as at December 2022

View a demo of this app [here](https://reactivities-course.fly.dev).   You just need to register a user and sign in to see it in action.  

You can see how this app was made by checking out the Udemy course for this here (with discount)

[Udemy course](https://www.udemy.com/course/complete-guide-to-building-an-app-with-net-core-and-react/?couponCode=REGITHUB)

If you are looking for the repository for the version of this app created on .Net 6.0 and Angular v12 then this is available here:

https://github.com/TryCatchLearn/Reactivities-v6

---------------------------------------------------------------------------------------------------------------------------------------------

 1  xdg-open .
    2  sudo apt install xdg-utils
    3  sudo apt install xdg-utils --fix-missing
    4  xdg-open .
    5  sudo update-alternatives --config x-www-browser
    6  ls -l /usr/share/applications/*.desktop | grep -i browser
    7  sudo update-alternatives --config x-www-browser
    8  xdg-settings set default-web-browser firefox.desktop
    9  xdg-open .
   10  sudo update-alternatives --config x-www-browser
   11  echo $PATH
   12  xdg-settings set default-web-browser firefox.desktop
   13  xdg-open .
   14  sudo apt-get install --reinstall xdg-utils
   15  xdg-open .
   16  xdg-open . --browser firefox
   17  nautilus .
   18  sudo apt install nautilus
   19  xdg-open . --browser firefox
   20  nautilus .
   21  xdg-open . --browser firefox
   22  xdg-settings set default-web-browser firefox.desktop
   23  sudo apt update
   24  sudo apt upgrade
   25  nautilus .
   26  xdg-open .
   27  xdg-settings set default-web-browser firefox.desktop
   28  sudo apt install nautilus
   29  nautilus .
   30  xdg-open .
   31  LS -LA
   32  ls -l
   33  ls -la
   34  code .
   35  xdg-open .
   36  ls -la
   37  cd code
   38  git init
   39  rm -fd .git/
   40  sudo rm -fd .git/
   41  sudo rm -rf .git/
   42  git status
   43  code .
   44  sudo snap install code
   45  curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh
   46  curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
   47  source ~/.bashrc
   48  nvm list-remote
   49  nvm install v21.7.2
   50  nvm list
   51  node -v
   52  touch hello.js
   53  sudo nano hello.js
   54  node hello.js
   55  npm create vite@latest
   56  ls
   57  cd react/
   58  npm install
   59  npm run dev
   60  npm run dev --host
   61  ls -la
   62  cd code

---------------------------------------------------------------------------------------------------------------------------------------------

 98  sudo apt update
   99  sudo apt install -y apt-transport-https ca-certificates curl software-properties-common
  100  wget https://packages.microsoft.com/config/ubuntu/$(lsb_release -rs)/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
  101  sudo dpkg -i packages-microsoft-prod.deb
  102  sudo apt update
  103  sudo apt install -y dotnet-sdk-7.0
  104  dotnet --version
  105  lsb_release -a
  106  sudo apt-get update &&   sudo apt-get install -y dotnet-sdk-7.0
  107  sudo apt-get update &&   sudo apt-get install -y aspnetcore-runtime-7.0
  108  dotnet --version
  109  echo $PATH
  110  source ~/.bashrc
  111  dotnet --version
  112  cd /usr/share/dotnet
  113  ls
  114  dotnet --version
  115  cd sdk
  116  ls
  117  dotnet --version
  118  echo $PATH
  119  dotnet --list-sdks
  120  cd ..
  121  ls
  122  cd home/
  123  cd co
  124  cd code
  125  ls
  126  cd markm/
  127  cd code/
  128  cd Reactivities/
  129  ls
  130  dot net run
  131  dotnet run
  132  dotnet --list-sdks
  133  echo $PATH
  134  source ~/.bashrc
  135  dotnet run
  136  env | grep DOTNET
  137  echo $DOTNET_ROOT
  138  cd ..
  139  ls
  140  ls -la
  141  unset DOTNET_ROOT
  142  unset PATH
  143  source ~/.bashrc
  144  export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/usr/lib/wsl/lib:/mnt/c/Windows/system32:/mnt/c/Windows:/mnt/c/Windows/System32/Wbem:/mnt/c/Windows/System32/WindowsPowerShell/v1.0/:/mnt/c/Windows/System32/OpenSSH/:/mnt/c/Program Files/NVIDIA Corporation/NVIDIA NvDLISR:/mnt/c/Program Files (x86)/NVIDIA Corporation/PhysX/Common:/mnt/c/Users/manhn/AppData/Local/Microsoft/WindowsApps:/snap/bin"
  145  source ~/.bashrc
  146  dotnet --version
  147  ls -la /usr/share/dotnet
  148  echo $PATH
  149  dotnet --version
  150  ls -l /usr/share/dotnet/sdk
  151  export PATH="/usr/share/dotnet:$PATH"
  152  source ~/.bashrc
  153  dotnet --version
  154  ls -l /usr/share/dotnet/dotnet
  155  ls -l /usr/share/dotnet
  156  /usr/share/dotnet/dotnet --version
  157  /usr/share/dotnet --version
  158  ls -l /usr/share/dotnet/sdk/7.0.407/
  159  export PATH="/usr/share/dotnet/sdk/7.0.407:$PATH"
  160  dotnet --version
  161  cd ..
  162  export PATH="/usr/share/dotnet/sdk/7.0.407:$PATH"
  163  dotnet --version
  164  source ~/.bashrc
  165  dotnet --version
  166  ls -l /usr/share/dotnet/sdk/7.0.407/
  167  echo $PATH
  168  ls -l /usr/share/dotnet/sdk/7.0.407/dotnet
  169  ls -l /usr/share/dotnet/sdk/7.0.407/
  170  /usr/share/dotnet/sdk/7.0.407/dotnet --version
  171  sudo apt-get install -y dotnet-runtime-7.0
  172  dotnet --version
  173  uname -m
  174  cat /etc/alpine-release
  175  xdg-open .
  176  wget https://download.visualstudio.microsoft.com/download/pr/bd9f066f-c0cf-495f-95bc-c3b96c9cf06e/ec93222e82bca1aa14590beb8a73625c/dotnet-sdk-7.0.407-linux-x64.tar.gz
  177  cd ~
  178  wget https://download.visualstudio.microsoft.com/download/pr/bd9f066f-c0cf-495f-95bc-c3b96c9cf06e/ec93222e82bca1aa14590beb8a73625c/dotnet-sdk-7.0.407-linux-x64.tar.gz
  179  ls
  180  mkdir -p $HOME/dotnet && tar zxf dotnet-sdk-7.0.407-linux-x64.tar.gz -C $HOME/dotnet
  181  export DOTNET_ROOT=$HOME/dotnet
  182  export PATH=$PATH:$HOME/dotnet
  183  dotnet --version
  184  source ~/.bashrc
  185  dotnet --version
  186  mkdir -p $HOME/dotnet && tar zxf dotnet-sdk-7.0.407-linux-x64.tar.gz -C $HOME/dotnet
  187  export DOTNET_ROOT=$HOME/dotnet
  188  export PATH=$PATH:$HOME/dotnet
  189  source ~/.bashrc
  190  dotnet --version
  191  echo 'export DOTNET_ROOT=$HOME/dotnet' >> ~/.bashrc
  192  echo 'export PATH=$PATH:$HOME/dotnet' >> ~/.bashrc
  193  source ~/.bashrc
  194  dotnet --version
  195  ls $HOME/dotnet
  196  echo $DOTNET_ROOT
  197  echo $PATH
  198  which dotnetbob@test.com
  204  $HOME/dotnet/dotnet --version
  205  dotnet --version
  206  export PATH="$HOME/dotnet:$PATH"
  207  source ~/.bashrc
  208  dotnet --version
  209  cd ~
  210  cd code/
  211  cd Reactivities/
  212  dotnet run
  213  ls
  214  cd API/
  215  dotnet run
  216  sudo apt update
  217  cd ~
  218  sudo apt update
  219  sudo apt install postgresql postgresql-contrib
  220  sudo systemctl start postgresql.service
  221  sudo -i -u postgres
  222  sudo -u postgres psql
  223  cd code/
  224  cd Reactivities/
  225  cd API/
  226  dotnet run
  227  export PATH="$HOME/dotnet:$PATH"
  228  dotnet run
  229  sudo -u postgres createuser --interactive
  230  sudo -u postgres psql
  231  dotnet run
  232  sudo -u postgres psql
  233  dotnet run
  234  history

  ---------------------------------------------------------------------------------------------------------------------------------------------

  223  cd ~
  224  psql -U admin -d reactivities
  225  psql -U admin -d reactivities -W
  226  psql -h localhost -p 5432 -U admin -d reactivities -W (admin secret reactivities)
  227  history
  
-------------------------------------------------------------------------------

  markm@MarkM:~$ psql -h localhost -p 5432 -U admin -d reactivities -W
Password:
psql (14.11 (Ubuntu 14.11-0ubuntu0.22.04.1))
SSL connection (protocol: TLSv1.3, cipher: TLS_AES_256_GCM_SHA384, bits: 256, compression: off)
Type "help" for help.

reactivities=# \dt
               List of relations
 Schema |         Name          | Type  | Owner
--------+-----------------------+-------+-------
 public | Activities            | table | admin
 public | ActivityAttendees     | table | admin
 public | AspNetRoleClaims      | table | admin
 public | AspNetRoles           | table | admin
 public | AspNetUserClaims      | table | admin
 public | AspNetUserLogins      | table | admin
 public | AspNetUserRoles       | table | admin
 public | AspNetUserTokens      | table | admin
 public | AspNetUsers           | table | admin
 public | Comments              | table | admin
 public | Photos                | table | admin
 public | UserFollowings        | table | admin
 public | __EFMigrationsHistory | table | admin
(13 rows)

-------------------------------------------------------------------------------

reactivities=# SELECT column_name
FROM information_schema.columns
WHERE table_name = 'AspNetUsers';
     column_name
----------------------
 EmailConfirmed
 PhoneNumberConfirmed
 TwoFactorEnabled
 LockoutEnd
 LockoutEnabled
 AccessFailedCount
 NormalizedEmail
 Id
 SecurityStamp
 ConcurrencyStamp
 PhoneNumber
 PasswordHash
 DisplayName
 Bio
 UserName
 NormalizedUserName
 Email
(17 rows)

------------------------------------------------------------------------------- 

reactivities=# SELECT "UserName", "DisplayName", "Email"
FROM "AspNetUsers";
 UserName  | DisplayName |        Email
-----------+-------------+---------------------
 bob       | Bob         | bob@test.com (Pa$$w0rd)
 jane      | Jane        | jane@test.com (Pa$$w0rd)
 tom       | Tom         | tom@test.com (Pa$$w0rd)
 manhng132 | Manh Nguyen | manhng132@gmail.com (Abc@12345678 // [RegularExpression("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}$", ErrorMessage = "Password must be complex")])
(4 rows)

------------------------------------------------------------------------------- 

Ubuntu WSL Win 11
https://apps.microsoft.com/detail/9pdxgncfsczv?rtc=1&activetab=pivot%3Aoverviewtab&hl=en-us&gl=US

Install dotnet 7
https://learn.microsoft.com/en-us/dotnet/core/install/linux-ubuntu-install?pivots=os-linux-ubuntu-2204&tabs=dotnet7

Install NVM and Node
https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-22-04#option-3-installing-node-using-the-node-version-manager

nvm list
-> v14.15.2
v16.13.1
nvm alias default 14.15.2
nvm alias default v14.15.2
nvm use default
not
nvm use 14.15.2

Install PostgresSQL
https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-22-04

Install SSH for gitlab
https://gorails.com/setup/ubuntu/22.04

git config --global color.ui true
git config --global user.name "maearon"
git config --global user.email "manhng132@gmail.com"
ssh-keygen -t ed25519 -C "manhng132@gmail.com"

reactivities=# \q
markm@MarkM:~$ git config --global color.ui true
markm@MarkM:~$ git config --global user.name "maearon"
markm@MarkM:~$ git config --global user.email "manhng132@gmail.com"
markm@MarkM:~$ ssh-keygen -t ed25519 -C "manhng132@gmail.com"
Generating public/private ed25519 key pair.
Enter file in which to save the key (/home/markm/.ssh/id_ed25519):
Created directory '/home/markm/.ssh'.
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/markm/.ssh/id_ed25519
Your public key has been saved in /home/markm/.ssh/id_ed25519.pub
The key fingerprint is:
SHA256:i8cMCO1yD3Sos3Fli68aUuggDJaPdW12zEDnKOVjzNw manhng132@gmail.com
The key's randomart image is:
+--[ED25519 256]--+
|      .+ .       |
|  .. .B O        |
|.o..+o+% E       |
|+.+=.*=.o        |
|+o*.B o S        |
|+. B + = .       |
|..o   + =        |
| . . . .         |
|  ...            |
+----[SHA256]-----+

mmarkm@MarkM:~$ cat ~/.ssh/id_ed25519.pub
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDPOMC4kBozdZJT1HwbtnmNpumf2tufdKRSnvXmtB3tZ manhng132@gmail.com
markm@MarkM:~$ ssh -T git@gitlab.com
The authenticity of host 'gitlab.com (172.65.251.78)' can't be established.
ED25519 key fingerprint is SHA256:eUXGGm1YGsMAS7vkcx6JOJdOGHPem5gQp4taiCfCLB8.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'gitlab.com' (ED25519) to the list of known hosts.
Welcome to GitLab, @maearon!
markm@MarkM:~$ ssh -T git@gitlab.com
Welcome to GitLab, @maearon!
markm@MarkM:~/code/Reactivities$ git remote add origin-gitlab git@gitlab.com:maearon/reactivities.git
markm@MarkM:~/code/Reactivities$ git remote -v
origin  https://github.com/TryCatchLearn/Reactivities.git (fetch)
origin  https://github.com/TryCatchLearn/Reactivities.git (push)
origin-gitlab   git@gitlab.com:maearon/reactivities.git (fetch)
origin-gitlab   git@gitlab.com:maearon/reactivities.git (push)
markm@MarkM:~/code/Reactivities$ git push -u origin-gitlab HEAD:main (https://gitlab.com/maearon/reactivities)

Navigate to Repository Settings:

Go to your GitLab repository by visiting the URL: https://gitlab.com/maearon/reactivities.
Click on the "Settings" option in the left sidebar.
Navigate to Repository Branches Settings:

In the settings menu, find and click on the "Repository" option.
Then, click on the "Allowed to force push" Toggle Switch.

markm@MarkM:~/code/Reactivities$ git push -u origin-gitlab HEAD:main -f
